package id.dimas.challenge8

import android.app.Application
import id.dimas.challenge8.helper.DataStoreManager
import id.dimas.challenge8.repo.MovieRepo
import id.dimas.challenge8.repo.UserRepo
import id.dimas.challenge8.service.TMDBClient
import id.dimas.challenge8.viewmodel.HomeViewModel
import id.dimas.challenge8.viewmodel.LoginViewModel
import id.dimas.challenge8.viewmodel.RegisterViewModel
import id.dimas.challenge8.viewmodel.SplashViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    val appModule = module {
        single { DataStoreManager(androidContext()) }
        single { TMDBClient.instance }
        single { MovieRepo(get()) }
        single { UserRepo(androidContext()) }
    }

    val viewModelModule = module {
        single { SplashViewModel(get()) }
        single { LoginViewModel(get(), get()) }
        single { HomeViewModel(get(), get()) }
        single { RegisterViewModel(get()) }

    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, viewModelModule))
        }
    }
}