package id.dimas.challenge8.service


import id.dimas.challenge8.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface TMDBApiService {

    @GET("movie/popular")
    suspend fun getAllMovie(@Query("api_key") key: String): MovieResponse

//    @GET("movie/{movie_id}")
//    suspend fun getDetailMovie(
//        @Path("movie_id") movieId: Int,
//        @Query("api_key") key: String
//    ): DetailMovieItem

}