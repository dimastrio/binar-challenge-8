package id.dimas.challenge8

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import id.dimas.challenge8.viewmodel.LoginViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeData()
        setContent {
            LoginPage()
        }
    }

    private fun observeData() {
        viewModel.succesMessage.observe(this) {
            messageToast(it)
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        viewModel.failedMessage.observe(this) {
            messageToast(it)
        }

    }

    private fun messageToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    @Preview(showSystemUi = true)
    @Composable
    fun LoginPage() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val (
                tv_login,
                iv_logo,
                ed_email,
                ed_password,
                btn_login,
                rowRegister
            ) = createRefs()

            var email by remember { mutableStateOf("") }
            var password by remember { mutableStateOf("") }
//        if (name.isNotEmpty()) {
//            Text(
//                text = "Hello, $name!",
//                modifier = Modifier.padding(bottom = 8.dp)
//            )
//        }
            Text(
                text = "LOGIN",
                style = TextStyle(
                    textAlign = TextAlign.Center,
                    fontSize = 40.sp,
                    fontWeight = FontWeight.Bold,
                    color = colorResource(id = R.color.grey_dark)
                ),
                modifier = Modifier
                    .padding(
                        top = 40.dp,
                        bottom = 40.dp
                    )
                    .constrainAs(tv_login) {
                        top.linkTo(parent.top, margin = 16.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }

            )

            Image(
                painter = painterResource(id = R.drawable.tmdb_logo),
                contentDescription = "Logo",
                modifier = Modifier
                    .size(130.dp)
                    .padding(
                        bottom = 30.dp
                    )
                    .constrainAs(iv_logo) {
                        top.linkTo(tv_login.bottom, margin = 15.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )

            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                maxLines = 1,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
                modifier = Modifier
                    .constrainAs(ed_email) {
                        top.linkTo(iv_logo.bottom, margin = 10.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                colors = TextFieldDefaults
                    .outlinedTextFieldColors(
                        focusedBorderColor = colorResource(
                            id = R.color.blue
                        ),
                        focusedLabelColor = colorResource(
                            id = R.color.blue
                        ),
                        cursorColor = colorResource(
                            id = R.color.blue
                        )

                    )
            )

            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                maxLines = 1,
                visualTransformation = PasswordVisualTransformation(),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                modifier = Modifier
                    .padding(
                        bottom = 20.dp
                    )
                    .constrainAs(ed_password) {
                        top.linkTo(ed_email.bottom, margin = 10.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                colors = TextFieldDefaults
                    .outlinedTextFieldColors(
                        focusedBorderColor = colorResource(
                            id = R.color.blue
                        ),
                        focusedLabelColor = colorResource(
                            id = R.color.blue
                        ),
                        cursorColor = colorResource(
                            id = R.color.blue
                        )

                    )
            )


            Button(
                onClick = {
                    viewModel.checkUserFromDb(email, password)
                    Log.d("ButtonClick", "Button berhasil diklik")
                },
                modifier = Modifier
                    .width(300.dp)
                    .height(50.dp)
                    .constrainAs(btn_login) {
                        bottom.linkTo(rowRegister.top, margin = 10.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(R.color.blue))
            ) {
                Text(
                    text = "Login",
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                    color = Color.White
                )
            }

            Row(
                modifier = Modifier
                    .padding(
                        top = 10.dp
                    )
                    .constrainAs(rowRegister) {
                        bottom.linkTo(parent.bottom, margin = 30.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            ) {
                Text(
                    text = "Belum Punya Akun?",
                    modifier = Modifier
                        .padding(
                            end = 5.dp
                        ),
                )
                ClickableText(
                    text = AnnotatedString(
                        text = "Daftar",
                    ),
                    onClick = {
                        val intent = Intent(applicationContext, RegisterActivity::class.java)
                        startActivity(intent)
                        finish()
                    },
                    style = TextStyle(
                        color = colorResource(id = R.color.blue)
                    )
                )
            }

        }
    }
}