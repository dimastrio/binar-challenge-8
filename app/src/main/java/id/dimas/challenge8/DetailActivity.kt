package id.dimas.challenge8

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DetailPage()
        }
    }

    @Preview(showSystemUi = true)
    @Composable
    fun DetailPage() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
        ) {

            val (
                tv_detail,
                iv_movie,
                tv_movie_title,
                tv_movie_date,
                tv_description
            ) = createRefs()


            Text(
                text = "Detail",
                style = TextStyle(
                    textAlign = TextAlign.Center,
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Medium
                ),
                modifier = Modifier
                    .constrainAs(tv_detail) {
                        start.linkTo(parent.start)
                        top.linkTo(parent.top, margin = 25.dp)
                        end.linkTo(parent.end)

                    }
            )

            Image(
                painter = painterResource(id = R.drawable.ic_launcher_background),
                contentDescription = "Movie Poster",
                modifier = Modifier
                    .size(180.dp)
                    .constrainAs(iv_movie) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(tv_detail.bottom, margin = 40.dp)
                    }
            )

            Text(
                text = "Jurasic World Dominion",
                style = TextStyle(
                    textAlign = TextAlign.Center,
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Medium
                ),
                modifier = Modifier
                    .constrainAs(tv_movie_title) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(iv_movie.bottom, margin = 20.dp)
                    }
            )

            Text(
                text = "01 Juni 2022",
                style = TextStyle(
                    textAlign = TextAlign.Center,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Normal
                ),
                modifier = Modifier
                    .padding(
                        top = 10.dp
                    )
                    .constrainAs(tv_movie_date) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(tv_movie_title.bottom)
                    }
            )

            Text(
                text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                style = TextStyle(
                    textAlign = TextAlign.Center,
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Normal
                ),
                modifier = Modifier
                    .padding(
                        start = 30.dp,
                        end = 30.dp,
                        top = 20.dp
                    )
                    .constrainAs(tv_description) {
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                        top.linkTo(tv_movie_date.bottom)
                    }
            )


        }
    }
}