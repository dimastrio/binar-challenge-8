package id.dimas.challenge8.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.dimas.challenge8.helper.DataStoreManager

class SplashViewModel(private val pref: DataStoreManager) : ViewModel() {

    fun getLogin(): LiveData<Boolean> {
        return pref.prefLogin().asLiveData()
    }

}