package id.dimas.challenge8.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.dimas.challenge8.helper.DataStoreManager
import id.dimas.challenge8.repo.UserRepo
import kotlinx.coroutines.launch

class LoginViewModel(private val pref: DataStoreManager, private val userRepo: UserRepo) :
    ViewModel() {

    private var _failedMessage = MutableLiveData<String>()
    val failedMessage: LiveData<String> get() = _failedMessage

    private var _succesMessage = MutableLiveData<String>()
    val succesMessage: LiveData<String> get() = _succesMessage

    fun checkUserFromDb(email: String, password: String) {
        viewModelScope.launch {
            val result = userRepo.checkEmailUser(email)
            if (result != null) {
                if (result.email == email && result.password == password) {
                    _succesMessage.value = "Login Berhasil"
                    pref.setData(result.userId!!, result.username, result.email, result.password)
                } else {
                    _failedMessage.value = "Username atau password kamu salah"
                }
            } else {
                _failedMessage.value = "Akun belum terdaftar"
            }
        }
    }


}