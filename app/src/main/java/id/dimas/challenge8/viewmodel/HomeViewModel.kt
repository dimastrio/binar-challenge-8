package id.dimas.challenge8.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.*
import id.dimas.challenge8.helper.DataStoreManager
import id.dimas.challenge8.model.MovieItem
import id.dimas.challenge8.model.Resource
import id.dimas.challenge8.repo.MovieRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val pref: DataStoreManager, private val movieRepo: MovieRepo) :
    ViewModel() {

    private var _succesMessage = MutableLiveData<String>()
    val succesMessage: LiveData<String> get() = _succesMessage

    var movieListResponse: List<MovieItem> by mutableStateOf(listOf())

    fun getUsername(): LiveData<String> {
        return pref.getUsername().asLiveData()
    }

    fun logout() {
        viewModelScope.launch {
            pref.cleanPref()
            _succesMessage.value = "Logout"
        }
    }

    fun getAllMovie(key: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getMovie(key)))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occured!"))
        }
    }
}