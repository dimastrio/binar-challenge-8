package id.dimas.challenge8.repo

import android.content.Context
import id.dimas.challenge8.database.MovieDatabase
import id.dimas.challenge8.database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {

    private val mDb = MovieDatabase.getInstance(context)

    suspend fun updateUser(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.updateUser(user)
    }

    suspend fun insertUser(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.insertUser(user)
    }

    suspend fun checkEmailUser(email: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.checkEmailUser(email)
    }

}