package id.dimas.challenge8

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import coil.compose.rememberImagePainter
import id.dimas.challenge8.model.MovieItem
import id.dimas.challenge8.viewmodel.HomeViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeData()
        setContent {
            HomePage()
        }
    }

    var username: String = ""

    private fun observeData() {
        viewModel.getUsername().observe(this) {
            username = it
        }
        viewModel.succesMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }
    }


    @Preview(showSystemUi = true)
    @Composable
    fun HomePage() {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
        ) {

            var usernameHome by remember { mutableStateOf("") }
            usernameHome = username

            val (
                tv_username,
                btn_logout,
                tv_home,
                lz_movie
            ) = createRefs()

            Text(
                text = "Hello, $usernameHome",
                style = TextStyle(
                    textAlign = TextAlign.Start,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Medium
                ),
                modifier = Modifier
                    .constrainAs(tv_username) {
                        start.linkTo(parent.start, margin = 25.dp)
                        top.linkTo(parent.top, margin = 25.dp)
                    }
            )

            ClickableText(
                text = AnnotatedString(
                    text = "Logout"
                ),
                onClick = {
                    viewModel.logout()
                    val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                },
                style = TextStyle(
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Medium
                ),
                modifier = Modifier
                    .constrainAs(btn_logout) {
                        top.linkTo(parent.top, margin = 25.dp)
                        end.linkTo(parent.end, margin = 25.dp)
                    }
            )

            Text(
                text = "Home",
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Medium,
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier
                    .constrainAs(tv_home) {
                        top.linkTo(tv_username.bottom, margin = 25.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )

        }
    }

    @Composable
    fun MovieList(movieList: List<MovieItem>) {
        LazyColumn {
            itemsIndexed(items = movieList) { index, item ->
                MovieCard(movie = item)
            }
        }
    }


    //    @Preview(showSystemUi = false)
    @Composable
    fun MovieCard(movie: MovieItem) {
        Card(
            elevation = 5.dp,
            modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth()
                .wrapContentHeight()
        ) {
            Row(
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxSize()
            ) {
                Image(
                    painter = rememberImagePainter(
                        data = "https://image.tmdb.org/t/p/w500${movie.posterPath}"
                    ),
                    contentDescription = "Poster Movie"
                )
                Text(
                    text = movie.originalTitle
                )
            }
        }
    }
}