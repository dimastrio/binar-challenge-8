package id.dimas.challenge8.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
